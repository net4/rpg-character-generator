﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Character_Generator
{
    /// <summary>
    /// When a character object is created, the base stats are generated at random. 
    /// Each class also get bonus points generated randomly after what fits the class.
    /// There are lists of dead and alive characters wich also can be accesed. 
    /// </summary>
    public abstract class Character
    {
        //List that contains all created characters
        public static List<Character> aliveCharacters = new List<Character>();
        public static List<Character> deadCharacters = new List<Character>();
        //Added random class for creating of stats
        public Random rnd = new Random();

        public string Name { get; set; }
        public int Hp { get; set; }
        public int Strength { get; set; }
        public int Wisdom { get; set; }
        public int Charsima { get; set; }
        public int Agility { get; set; }
        public string Type { get; set; }
        public bool Alive { get; set; }
        public bool HumanShape { get; set; }

        //Constructor that takes no arguments
        public Character()
        {
            Name = "Unknown";
            Hp = 100;
            Strength = rnd.Next(1,11);
            Wisdom = rnd.Next(1, 11);
            Charsima = rnd.Next(1, 11);
            Agility = rnd.Next(1, 11);
            Type = "Not known";
            Alive = true;
            aliveCharacters.Add(this);
        }

        //Constructor for creating object
        public Character(string name)
        {
            Name = name;
            Hp = rnd.Next(100, 140);
            Strength = rnd.Next(10, 30);
            Wisdom = rnd.Next(10, 30);
            Charsima = rnd.Next(10, 30);
            Agility = rnd.Next(10, 30);
            Alive = true;
        }
        /// <summary>
        /// Method to attack a character, and do damage and possibly kill the character
        /// Also returns a string with some text about the attack and outcome.
        /// </summary>
        /// <param name="character">Set character object to be attacked</param>
        public abstract string Attack(Character character);
        /// <summary>
        /// Method returns string with information about how the character moves. Does nothing exciting... yet?
        /// </summary>
        public abstract string Move();
        /// <summary>
        /// Method to kill of a character. Once the character is dead he will be moved from the alive list to the dead list.
        /// Also the Alive property gets set to false.
        /// </summary>
        public void Die()
        {
            this.Alive = false;
            aliveCharacters.Remove(this);
            deadCharacters.Add(this);
        }
        /// <summary>
        /// Method returns a string with all character stats displayed.
        /// </summary>
        public string Display()
        {
            return $"Name: {Name}\n" +
                $"Type: {Type}\n" +
                $"Hp: {Hp}\n" +
                $"Strength: {Strength}\n" +
                $"Wisdom: {Wisdom}\n" +
                $"Charisma: {Charsima}\n" +
                $"Agility: {Agility}\n";
        }
        /// <summary>
        /// Super simple method to just return a string with information about the newest created object.
        /// </summary>
        /// <returns></returns>
        public static string DisplayNewest()
        {
            return aliveCharacters[aliveCharacters.Count()-1].Display();
        }

    }
}
