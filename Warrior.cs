﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Character_Generator
{
    public class Warrior : Character
    {
        public Warrior() : base() { }
        public Warrior(string name):base(name) {
            Strength += rnd.Next(5, 31);
            Hp += rnd.Next(5, 31);
            Type = "Warrior";
            aliveCharacters.Add(this);
        }
        public override string Attack(Character character)
        {
            if ((character.Hp -= Strength) > 0)
            {
                character.Hp -= Strength;
                return $"{Name} swings his sword against {character.Name} and does {Strength} damage!";
            }
            else
            {
                character.Hp -= Strength;
                character.Die();
                return $"{Name} swings his sword and executes {character.Name} !";

            }
        }
        public  override string Move()
        {
            return $"{Name} moves slowly carrying his sword with both hands, ready for action";
        }
        public static void Create(string name)
        {
            new Warrior(name);
        }
    }
}
