﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Character_Generator
{
    public class Druid : Character
    {
        public Druid() : base() { }
        public Druid(string name) : base(name)
        {
            Agility += rnd.Next(5, 31);
            Charsima += rnd.Next(5, 31);
            Type = "Druid";
            HumanShape = true;
            aliveCharacters.Add(this);
        }

        public override string Attack(Character character)
        {
            if (HumanShape == true) { Console.WriteLine($"{Name} shapeshifts into a cat"); }
            if ((character.Hp -= Strength + Agility) > 0)
            {
                character.Hp -= Strength + Agility;
                return $"{Name} runs towards {character.Name} and does {Strength + Agility} damage!";
            }
            else
            {
                character.Hp -= Strength + Agility;
                character.Die();
                return $"{Name} jumps and bites {character.Name} neck! Doing fatal damage! RIP";

            }
        }
        public override string Move()
        {
            return $"{Name} morphes into a cat, moving elegantly and arrogant...";
        }
        public bool Shapeshift()
        {
            if (HumanShape == true)
            {
                return HumanShape = false;
            } else 
            { return HumanShape = true; 
            }
        }
        public static void Create(string name)
        {
            new Druid(name);
        }
    }
}
