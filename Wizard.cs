﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Character_Generator
{
    public class Wizard : Character
    {
        public Wizard() : base() { }
        public Wizard(string name):base(name)
        {
            Wisdom += rnd.Next(10,20);
            Type = "Wizard";
            aliveCharacters.Add(this);
        }
        public override string Attack(Character character)
        {
            if ((character.Hp -= Wisdom) > 0)
            {
                character.Hp -= Wisdom;
                return $"{Name} swings his wand against {character.Name} and does {Wisdom} magic damage!";
            }
            else
            {
                character.Hp -= Wisdom;
                character.Die();
                return $"{Name} points his wand towards {character.Name}. Suddenly {character.Name} falls to the ground dead. RIP";

            }
        }
        public override string Move()
        {
            return $"{Name} confidently moves... Always looking for trouble. ";
        }
        public static void Create(string name)
        {
            new Wizard(name);
        }
    }
}
