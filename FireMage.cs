﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Character_Generator
{
    public class FireMage : Wizard
    {
        public FireMage() : base() { }
        public FireMage(string name) : base(name)
        {
            Wisdom += rnd.Next(5, 15);
            Charsima += rnd.Next(15, 31);
            Type = "Fire Mage (Wizard)";
        }
        public override string Attack(Character character)
        {
            if ((character.Hp -= Wisdom) > 0)
            {
                character.Hp -= Wisdom;
                return $"{Name} swings his wand against {character.Name} and does {Wisdom} fire damage!";
            }
            else
            {
                character.Hp -= Wisdom;
                character.Die();
                return $"{Name} points his wand towards {character.Name}. An inferno of flames sourround {character.Name}. All that remains are a pile of ashes. RIP";

            }
        }
        public override string Move()
        {
            return $"{Name} walks around happily...";
        }
        public static void Create(string name)
        {
            new FireMage(name);
        }
    }
}
