﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Character_Generator
{
    class Program
    {
        static void Main(string[] args)
        {
            Druid.Create("Felix");
            Wizard.Create("Thomas");

            Console.WriteLine(Character.DisplayNewest()); 

            Console.ReadLine();
        }
    }
}
