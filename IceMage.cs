﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Character_Generator
{
    public class IceMage : Wizard
    {
        public IceMage() : base() { }
        public IceMage(string name) : base(name)
        {
            Wisdom += rnd.Next(5, 15);
            Hp += rnd.Next(15, 31);
            Type = "Ice Mage (Wizard)";
        }
        public override string Attack(Character character)
        {
            if ((character.Hp -= Wisdom) > 0)
            {
                character.Hp -= Wisdom;
                return $"{Name} swings his popsticle against {character.Name} and does {Wisdom} ice damage!";
            }
            else
            {
                character.Hp -= Wisdom;
                character.Die();
                return $"{Name} points his popsticle towards {character.Name}. A storm of ice surrounds {character.Name}. His heart stops beating. RIP";

            }
        }
        public override string Move()
        {
            return $"{Name} walks around freezing...";
        }
        public static void Create(string name)
        {
            new IceMage(name);
        }
    }
}
