﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Character_Generator
{
    public class Tauren : Druid
    {
        public Tauren() : base() { }
        public Tauren(string name) : base(name)
        {
            Hp += rnd.Next(5, 15);
            Strength += rnd.Next(15, 31);
            Type = "Tauren (Druid)";
        }
        public override string Attack(Character character)
        {
            {
                if (HumanShape == true) { Console.WriteLine($"{Name} shapeshifts into a half human half cow form!"); }
                if ((character.Hp -= Strength + Agility) > 0)
                {
                    character.Hp -= Strength + Agility;
                    return $"{Name} punches {character.Name} and does {Strength + Agility} damage!";
                }
                else
                {
                    character.Hp -= Strength + Agility;
                    character.Die();
                    return $"{Name} says mooooooooo! And hits {character.Name} with a thunder fist! Doing fatal damage! RIP";

                }
            }
        }
        public static void Create(string name)
        {
            new Tauren(name);
        }
    }
}

