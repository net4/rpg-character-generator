﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Character_Generator
{
    public class Worgen : Druid
    {
        public Worgen() : base() { }
        public Worgen(string name) : base(name)
        {
            Agility += rnd.Next(5, 31);
            Strength += rnd.Next(5, 31);
            Type = "Worgen (Druid)";
        }
        public override string Attack(Character character)
        {
            {
                if (HumanShape == true) { Console.WriteLine($"{Name} shapeshifts into a werewolf"); }
                if ((character.Hp -= Strength + Agility) > 0)
                {
                    character.Hp -= Strength + Agility;
                    return $"{Name} jumps towards {character.Name} and does {Strength + Agility} damage!";
                }
                else
                {
                    character.Hp -= Strength + Agility;
                    character.Die();
                    return $"{Name} jumps and bites {character.Name} face! Doing fatal damage! RIP";

                }
            }
        }
        public static void Create(string name)
        {
            new Worgen(name);
        }
    }
}
